package com.denkening.pedidos.unit.domain.factory.nodomainmodel;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class NoDomainValorPedido {
    private Long total;
    private Long subTotal;
    private Long totalIva;
    private List<NoDomainIva> detalleIva;
}
