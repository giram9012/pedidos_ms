package com.denkening.pedidos.unit.domain.model;

import com.denkening.pedidos.unit.domain.factory.generator.ValorPedidoGenerator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ValorPedidoTest extends ValorPedidoGenerator {

    @Test
    void createValidValorPedido() {
        assertTrue(valorPedidoGenerator().isValid());
    }

    @Test
    void fieldsValorPedidoInvalid() {
        assertTrue(valorPedidoInvalidFGenerator().getError().size() == 3);
    }

    @Test
    void validFieldsInvalidValorPedido() {
        //Should be "Los valores en el valorPedido tienen insonsistencias"
        System.out.println(valorPedidoInvalidGenerator().getError());
        assertTrue(valorPedidoInvalidGenerator().getError().size() == 1);
    }

    @Test
    void validFieldValorPedidoInvalidFDetalleIva() {
        //Should be Valor de dinero no valido : -64025, Los valores en el valorPedido tienen insonsistencias
        int numDineroNeg = 3 * 2; // tarifas iva * (valor base + valor iva)
        System.out.println(valorPedidoDetalleIvaInvalidFGenerator().getError());
        assertTrue(valorPedidoDetalleIvaInvalidFGenerator().getError().size() == numDineroNeg + 1);
    }
}

