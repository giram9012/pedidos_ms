package com.denkening.pedidos.unit.domain.model;

import com.denkening.pedidos.unit.domain.factory.generator.PedidoGenerator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class PedidoTest extends PedidoGenerator {

    @Test
    void createValidPedido() {
        assertTrue(pedidoGenerator().isValid());
    }

    @Test
    void fieldsPedidoInvalid() {
        assertTrue(pedidoInvalidFGenerator().getError().size() == 5);
    }

    @Test
    void pedidoFValidProductosFInvalid() {
        // Should be Nombre del producto no es valido: 3,
        // Descripcion del producto no es valido: 37,
        // Cantidad debe ser minimo 1, Valor de dinero no valido : -34759,
        // "Existen inconsistencias en los productos y el  valor del pedido"
        // System.out.println(pedidoFValidAndProdInvalidFGenerator().getError());
        assertTrue(pedidoFValidAndProdInvalidFGenerator().getError().size() >= 1);
    }

    @Test
    void pedidoFValidValorPedidoFInvalid() {
        // Should be Valor de dinero no valido : -27915, Valor de dinero no valido : -27915,
        // Valor de dinero no valido : -27915, "Existen inconsistencias en los productos y el  valor del pedido"
        // System.out.println(pedidoFValidAndValorPedidoInvalidFGenerator().getError());
        assertTrue(pedidoFValidAndValorPedidoInvalidFGenerator().getError().size() >= 1);
    }
}
