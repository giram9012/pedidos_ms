package com.denkening.pedidos.unit.domain.factory.generator;

import com.denkening.pedidos.domain.model.iva.Iva;
import com.denkening.pedidos.domain.model.iva.IvaBuilder;
import com.denkening.pedidos.unit.domain.factory.nodomainmodel.NoDomainIva;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;

public class IvaGenerator extends ValueGenerator {
    IvaBuilder ivaBuilder = new IvaBuilder();

    public Validation<Seq<String>, Iva> ivaGenenator() {
        NoDomainIva iva = ivaGen();
        return ivaBuilder.build(iva.getTarifaIva(), iva.getValorBase(), iva.getValorIva());
    }

    public Validation<Seq<String>, Iva> ivaInvalidFGenenator() {
        NoDomainIva iva = ivaInvalidFGen();
        return ivaBuilder.build(iva.getTarifaIva(), iva.getValorBase(), iva.getValorIva());
    }

    public Validation<Seq<String>, Iva> ivaInvalidGenenator() {
        NoDomainIva iva = ivaGen();
        Long d = dineroGen();
        return ivaBuilder.build(iva.getTarifaIva(), d, d + 1);
    }
}
