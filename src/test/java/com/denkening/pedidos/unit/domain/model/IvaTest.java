package com.denkening.pedidos.unit.domain.model;

import com.denkening.pedidos.unit.domain.factory.generator.IvaGenerator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class IvaTest extends IvaGenerator {

    @Test
    void createValidIva() {
        assertTrue(ivaGenenator().isValid());
    }

    @Test
    void fiedlsIvaInvalid() {

        assertTrue(ivaInvalidFGenenator().getError().size() == 3);
    }

    @Test
    void validFieldsInvalidIva() {
/*        IvaError thrown = assertThrows(
            IvaError.class,
            () -> ivaInvalidGenenator(),
            "Should throw ivaError"
        );
        assertTrue(thrown.getMessage().contains(new IvaError().getMessage()));*/
        assertTrue(ivaInvalidGenenator().getError().size() == 1);

    }
}
