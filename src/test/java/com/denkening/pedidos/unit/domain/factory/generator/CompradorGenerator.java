package com.denkening.pedidos.unit.domain.factory.generator;

import com.denkening.pedidos.domain.model.comprador.Comprador;
import com.denkening.pedidos.domain.model.comprador.CompradorBuilder;
import com.denkening.pedidos.unit.domain.factory.nodomainmodel.NoDomainComprador;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;

public class CompradorGenerator extends ValueGenerator {

    CompradorBuilder compradorBuilder = new CompradorBuilder();

    public Validation<Seq<String>, Comprador> compradorGenerator() {
        NoDomainComprador comprador = compradorGen();
        return compradorBuilder.build(comprador.getId(), comprador.getNombre(), comprador.getTelefono());
    }

    public Validation<Seq<String>, Comprador> compradorInvalidFGenerator() {
        NoDomainComprador comprador = compradorInvalidFGen();
        return compradorBuilder.build(comprador.getId(), comprador.getNombre(), comprador.getTelefono());
    }

}
