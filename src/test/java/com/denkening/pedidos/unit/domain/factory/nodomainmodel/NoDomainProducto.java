package com.denkening.pedidos.unit.domain.factory.nodomainmodel;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NoDomainProducto {
    private String id;
    private String nombre;
    private String descripcion;
    private int cantidad;
    private Long precioUnidad;
    private NoDomainIva iva;
}