package com.denkening.pedidos.unit.domain.factory.generator;

import com.denkening.pedidos.domain.model.valorpedido.ValorPedido;
import com.denkening.pedidos.domain.model.valorpedido.ValorPedidoBuilder;
import io.vavr.Tuple3;
import io.vavr.Tuple4;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;

public class ValorPedidoGenerator extends ValueGenerator {
    ValorPedidoBuilder valorPedidoBuilder = new ValorPedidoBuilder();

    public Validation<Seq<String>, ValorPedido> valorPedidoGenerator() {
        Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedido = valorPedidoGen();
        return valorPedidoBuilder.build(valorPedido);
    }

    public Validation<Seq<String>, ValorPedido> valorPedidoInvalidFGenerator() {
        Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedido = valorPedidoIvalidFGen();
        return valorPedidoBuilder.build(valorPedido);
    }

    public Validation<Seq<String>, ValorPedido> valorPedidoInvalidGenerator() {
        Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedido = valorPedidoInvalidGen();
        return valorPedidoBuilder.build(valorPedido);
    }

    public Validation<Seq<String>, ValorPedido> valorPedidoDetalleIvaInvalidFGenerator() {
        Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedido = valorPedidoDetalleIvaInvalidFGen();
        return valorPedidoBuilder.build(valorPedido);
    }

}
