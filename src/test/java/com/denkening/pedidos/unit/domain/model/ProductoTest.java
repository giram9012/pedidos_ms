package com.denkening.pedidos.unit.domain.model;

import com.denkening.pedidos.unit.domain.factory.generator.ProductosGenerator;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class ProductoTest extends ProductosGenerator {

    @Test
    void createValidProducto() {
        assertTrue(productosGenerator().isValid());
    }

    @Disabled
    @Test
    void fieldsProductosInvalid() {
        int numOfFieldsCanFailInProd = 5;
        assertTrue(productosInvalidFGenerator().getError().size() == NUM_PROD_INVALIDOS_GEN * numOfFieldsCanFailInProd);
    }

    @Test
    void validFieldsInvalidProduct() {
        //All should be El precio del producto y el iva son inconsistentes
        //System.out.println(productosInvalidGenerator().getError());
        assertTrue(productosInvalidGenerator().getError().size() >= 1);
    }

    @Test
    void validFieldsProductoInvalidIvaOfSomeProduct() {
        // All should be Tarifa Iva no valida : 1, Valor de dinero no valido : -39209,
        // Valor de dinero no valido : -78810, El precio del producto y el iva son inconsistentes
        // System.out.println(productosIvaInvalidGenerator().getError());
        assertTrue(productosIvaInvalidGenerator().getError().size() >= 1);
    }
}
