package com.denkening.pedidos.unit.domain.factory.nodomainmodel;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NoDomainComprador {

    private String id;
    private String nombre;
    private String telefono;
}
