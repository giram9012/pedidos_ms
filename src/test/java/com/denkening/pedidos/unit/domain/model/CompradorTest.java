package com.denkening.pedidos.unit.domain.model;

import com.denkening.pedidos.domain.model.comprador.Comprador;
import com.denkening.pedidos.unit.domain.factory.generator.CompradorGenerator;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CompradorTest  extends CompradorGenerator {

    @Test
    void createValidComprador() {
        Validation<Seq<String>, Comprador> c = compradorGenerator();
        if (c.isValid())
            System.out.println(c.get().toString());
        else
            System.out.println(c.getError());
        assertTrue(c.isValid());
        // assertTrue(compradorGenerator().isValid());
    }

    @Test
    void fieldsCompradorInvalid() {
        assertTrue(compradorInvalidFGenerator().getError().size() == 3);
    }
}
