package com.denkening.pedidos.unit.domain.factory;

import com.denkening.pedidos.domain.factory.PedidoFactory;
import com.denkening.pedidos.domain.model.pedido.Pedido;
import com.denkening.pedidos.unit.domain.factory.generator.ValueGenerator;
import com.denkening.pedidos.unit.domain.factory.nodomainmodel.NoDomainPedido;
import com.google.gson.Gson;
import io.vavr.Tuple;
import io.vavr.Tuple4;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import net.joshka.junit.json.params.JsonFileSource;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;

import javax.json.JsonObject;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertTrue;

class PedidoFactoryTest extends ValueGenerator implements PedidoFactory<NoDomainPedido> {

    Gson gson = new Gson();

    @Override
    public Validation<Seq<String>, Pedido> of(NoDomainPedido pedido) {

        List productos = List.of(pedido.getProductos())
            .flatMap(noDomainProducts ->
                noDomainProducts.stream().map(
                    p -> Tuple.of(p.getId(), p.getNombre(), p.getDescripcion(), p.getCantidad(), p.getPrecioUnidad()
                        , Tuple.of(p.getIva().getTarifaIva(), p.getIva().getValorBase(), p.getIva().getValorIva()))
                ).collect(Collectors.toList())
            );

        List detalleIva = List.of(pedido.getValorPedido().getDetalleIva())
            .flatMap(noDomainIvas ->
                noDomainIvas.stream().map(
                    di -> Tuple.of(di.getTarifaIva(), di.getValorBase(), di.getValorIva())
                ).collect(Collectors.toList())
            );

        Tuple4 valorPedido = Tuple.of(pedido.getValorPedido().getTotal(), pedido.getValorPedido().getSubTotal(),
            pedido.getValorPedido().getTotalIva(), detalleIva);

        return of(
            pedido.getId(),
            pedido.getEstado(),
            ISODateTimeFormat.dateTimeParser().parseDateTime(pedido.getFechaCreacion()),
            pedido.getCompradorId(),
            pedido.getDireccion(),
            productos,
            valorPedido
        );
    }

    @DisplayName("Test and use case of Pedido Factory")
    @ParameterizedTest
    @JsonFileSource(resources = "/pedidoReqValido.json")
    void givenOneNoDomainPedidoObjectWhenItShouldBeConvertedToDomainPedidoThenConvertIt(JsonObject noDomainPedidoObj) {

        NoDomainPedido noDomainPedido = gson.fromJson(noDomainPedidoObj.toString(), NoDomainPedido.class);

        Validation<Seq<String>, Pedido> result = of(noDomainPedido);

        assertTrue(result.isValid());
    }

}
