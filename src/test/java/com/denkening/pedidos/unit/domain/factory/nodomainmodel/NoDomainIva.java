package com.denkening.pedidos.unit.domain.factory.nodomainmodel;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NoDomainIva {
    private int tarifaIva;
    private Long valorBase;
    private Long valorIva;
}