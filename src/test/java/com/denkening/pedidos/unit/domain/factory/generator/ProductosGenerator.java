package com.denkening.pedidos.unit.domain.factory.generator;

import com.denkening.pedidos.domain.model.producto.Producto;
import com.denkening.pedidos.domain.model.producto.ProductoBuilder;
import io.vavr.Tuple3;
import io.vavr.Tuple6;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;

public class ProductosGenerator extends ValueGenerator {

    public static final int NUM_PROD_INVALIDOS_GEN = 3;

    ProductoBuilder productoBuilder = new ProductoBuilder();

    public Validation<Seq<String>, List<Producto>> productosGenerator() {
        int numProd = randomNum(1, 11);
        List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productos = productosGen(numProd);
        return productoBuilder.build(productos);
    }

    public Validation<Seq<String>, List<Producto>> productosInvalidFGenerator() {
        List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productos =
            productosInvalidFGen(NUM_PROD_INVALIDOS_GEN);
        return productoBuilder.build(productos);
    }

    public Validation<Seq<String>, List<Producto>> productosInvalidGenerator() {
        int numProd = randomNum(1, 11);
        List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productos =
            productosInvalidGen(numProd);
        return productoBuilder.build(productos);
    }

    public Validation<Seq<String>, List<Producto>> productosIvaInvalidGenerator() {
        int numProd = randomNum(1, 11);
        List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productos =
            productosIvaInvalidGen(numProd);
        return productoBuilder.build(productos);
    }
}
