package com.denkening.pedidos.unit.domain.factory.generator;

import com.denkening.pedidos.domain.model.comprador.CompradorBuilder;
import com.denkening.pedidos.unit.domain.factory.nodomainmodel.*;
import com.github.javafaker.Faker;
import io.vavr.*;
import io.vavr.collection.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ValueGenerator {

    private Faker faker = new Faker();

    public List<Integer> tarifasIva = List.of(0, 5, 19);

    public String estadosPedidoGenerado = "GENERADO";

    public List<String> estadosPedido = List.of("GENERADO", "CONFIRMADO", "ANULADO");

    public String estadosPedidoGen() {
        return estadosPedido.get(randomNum(0, estadosPedido.length()));
    }

    public int tarifasIvaGen() {
        return tarifasIva.get(randomNum(0, 3));
    }

    public int tarifaIvaInvalidGen() {
        int ti = randomNum(0, 5);
        return tarifasIva.contains(ti) ? tarifaIvaInvalidGen() : ti;
    }

    public Long dineroGen() {
        return Long.valueOf(randomNum(100, 50000));
    }

    public Long dineroNegGen() {
        return Long.valueOf(randomNum(-50000, -50));
    }

    public int cantidadGen() {
        return randomNum(1, 20);
    }

    public int cantidadNegOrZGen() {
        return randomNum(-20, 0);
    }

    public String telefonoGen() {
        return faker.regexify(CompradorBuilder.VALID_TELEFONO_CHARS);
    }

    public String telefonoInvalidGen() {
        String tel = faker.phoneNumber().phoneNumber();
        return validateRegExp(CompradorBuilder.VALID_TELEFONO_CHARS, tel) ? telefonoInvalidGen() : tel;
    }


    // Iva ###############################################################
    public NoDomainIva ivaGen() {
        int ti = tarifasIvaGen();
        Long precioProd = dineroGen();
        Long valorBase = calcularValorBase(precioProd, ti);
        return new NoDomainIva(ti, valorBase, precioProd - valorBase);
    }

    public NoDomainIva ivaInvalidFGen() {
        int ti = tarifaIvaInvalidGen();
        Long precioProd = dineroNegGen();
        Long valorBase = calcularValorBase(precioProd, ti);
        return new NoDomainIva(ti, valorBase, precioProd + valorBase);
    }

    public Long calcularValorBase(Long precioProd, int trifaIva) {
        return Math.round(precioProd / (1 + (trifaIva / 100.0)));
    }
    // Iva ###############################################################


    // Comprador ###############################################################
    public NoDomainComprador compradorGen() {
        return new NoDomainComprador(UUID.randomUUID().toString(),
            faker.name().fullName(), telefonoGen());
    }

    public NoDomainComprador compradorInvalidFGen() {
        return new NoDomainComprador(faker.idNumber().valid(),
            faker.number().digit(), telefonoInvalidGen());
    }
    // Comprador ###############################################################


    // Producto ###############################################################
    public NoDomainProducto productoGen() {
        String id = UUID.randomUUID().toString();
        String nombre = faker.commerce().productName();
        String descripcion = faker.commerce().productName();
        int cantidad = cantidadGen();
        NoDomainIva iva = ivaGen();
        return new NoDomainProducto(id, nombre, descripcion, cantidad,
            iva.getValorBase() + iva.getValorIva(), iva);
    }

    public NoDomainProducto productoInvalidFGen() {
        String id = faker.number().digit();
        String nombre = faker.number().digits(1);
        String descripcion = faker.number().digits(2);
        int cantidad = cantidadNegOrZGen();
        NoDomainIva iva = ivaGen();
        return new NoDomainProducto(id, nombre, descripcion, cantidad,
            dineroNegGen(), iva);
    }

    public NoDomainProducto productoInvalidGen() {
        NoDomainProducto prod = productoGen();
        prod.setPrecioUnidad(dineroGen());
        return prod;
    }

    public NoDomainProducto productoIvaInvalidGen() {
        NoDomainProducto prod = productoGen();
        prod.setIva(ivaInvalidFGen());
        return prod;
    }

    public List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productosGen(int numProd) {
        java.util.List<NoDomainProducto> noDomainProductos =
            Stream.generate(this::productoGen).limit(numProd).collect(Collectors.toList());
        return createListVavrProductosFrom(noDomainProductos);
    }

    public List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productosInvalidFGen(int numProd) {
        java.util.List<NoDomainProducto> noDomainProductos =
            Stream.generate(this::productoInvalidFGen).limit(numProd).collect(Collectors.toList());
        return createListVavrProductosFrom(noDomainProductos);
    }

    public List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productosInvalidGen(int numProd) {
        java.util.List<NoDomainProducto> noDomainProductos =
            Stream.generate(this::productoInvalidGen).limit(numProd).collect(Collectors.toList());
        return createListVavrProductosFrom(noDomainProductos);
    }

    public List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productosIvaInvalidGen(int numProd) {
        java.util.List<NoDomainProducto> noDomainProductos =
            Stream.generate(this::productoIvaInvalidGen).limit(numProd).collect(Collectors.toList());
        return createListVavrProductosFrom(noDomainProductos);
    }

    public List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>>
    createListVavrProductosFrom(java.util.List<NoDomainProducto> noDomainProductos) {
        return List.of(noDomainProductos)
            .flatMap(noDomainProducts ->
                noDomainProducts.stream().map(
                    p -> Tuple.of(p.getId(), p.getNombre(), p.getDescripcion(), p.getCantidad(), p.getPrecioUnidad()
                        , Tuple.of(p.getIva().getTarifaIva(), p.getIva().getValorBase(), p.getIva().getValorIva()))
                ).collect(Collectors.toList())
            );
    }
    // Producto ###############################################################


    // ValorPedido ###############################################################
    public List<Tuple3<Integer, Long, Long>> detalleIvaGen() {
        return tarifasIva.map(
            ti -> {
                Long precioProd = dineroGen();
                Long valorBase = calcularValorBase(precioProd, ti);
                return Tuple.of(ti, valorBase, precioProd - valorBase);
            }
        );
    }

    public List<Tuple3<Integer, Long, Long>> detalleIvaInvalidFGen() {
        return tarifasIva.map(
            ti -> {
                Long precioProd = dineroNegGen();
                Long valorBase = calcularValorBase(precioProd, ti + 1);
                return Tuple.of(ti, valorBase, precioProd + valorBase);
            }
        );
    }

    public Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedidoGen() {
        List<Tuple3<Integer, Long, Long>> detalleIva = detalleIvaGen();
        Tuple2<Long, Long> subTotal_totalIva = detalleIva
            .foldLeft(Tuple.of(0L, 0L),
                (acc, iva) -> Tuple.of(acc._1 + iva._2, acc._2 + iva._3)); // iva._2 = iva.valorBase, iva._3 = iva.valorIva

        Long total = subTotal_totalIva._1 + subTotal_totalIva._2;
        Long subTotal = subTotal_totalIva._1;
        Long totalIva = subTotal_totalIva._2;
        return Tuple.of(total, subTotal, totalIva, detalleIva);
    }

    public Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedidoIvalidFGen() {
        List<Tuple3<Integer, Long, Long>> detalleIva = detalleIvaGen();
        Long dineroInvalido = dineroNegGen();
        return Tuple.of(dineroInvalido, dineroInvalido, dineroInvalido, detalleIva);
    }

    public Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedidoInvalidGen() {
        Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedido = valorPedidoGen();
        return Tuple.of(valorPedido._3, valorPedido._2, valorPedido._3, valorPedido._4);
    }

    public Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedidoDetalleIvaInvalidFGen() {
        Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedido = valorPedidoGen();
        List<Tuple3<Integer, Long, Long>> dIvaInvalido = detalleIvaInvalidFGen();
        return Tuple.of(valorPedido._1, valorPedido._2, valorPedido._3, dIvaInvalido);
    }
    // ValorPedido ###############################################################


    // Pedido ###############################################################
    public NoDomainPedido pedidoGen() {
        String id = UUID.randomUUID().toString();
        String estado = estadosPedidoGenerado;
        String fechaCreacion = DateTime.now().withZone(DateTimeZone.UTC).toString();
        String compradorId = UUID.randomUUID().toString();
        String direccion = faker.address().fullAddress();
        return new NoDomainPedido(id, estado, fechaCreacion, compradorId, direccion, null, null);
    }

    public NoDomainPedido pedidoInvalidFGen() {
        String id = faker.number().digit();
        String estado = faker.name().name();
        String fechaCreacion = DateTime.now().withZone(DateTimeZone.UTC).plusDays(1).toString();
        String compradorId = faker.number().digit();
        String direccion = "";
        return new NoDomainPedido(id, estado, fechaCreacion, compradorId, direccion, null, null);
    }
    // Pedido ###############################################################


    public int randomNum(int min, int max) {
        return faker.number().numberBetween(min, max);
    }

    public boolean validateRegExp(String pattern, String match) {
        Pattern pat = Pattern.compile(pattern);
        Matcher mat = pat.matcher(match);
        return mat.matches();
    }
}





















