package com.denkening.pedidos.unit.domain.factory.nodomainmodel;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class NoDomainPedido {
    private String id;
    private String estado;
    private String fechaCreacion;
    private String compradorId;
    private String direccion;
    private List<NoDomainProducto> productos;
    private NoDomainValorPedido valorPedido;
}
