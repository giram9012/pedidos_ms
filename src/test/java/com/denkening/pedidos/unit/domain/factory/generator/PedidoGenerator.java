package com.denkening.pedidos.unit.domain.factory.generator;

import com.denkening.pedidos.domain.model.pedido.Pedido;
import com.denkening.pedidos.domain.model.pedido.PedidoBuilder;
import com.denkening.pedidos.unit.domain.factory.nodomainmodel.NoDomainPedido;
import io.vavr.Tuple;
import io.vavr.Tuple3;
import io.vavr.Tuple4;
import io.vavr.Tuple6;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import org.joda.time.format.ISODateTimeFormat;

import static com.denkening.pedidos.unit.domain.factory.generator.ProductosGenerator.NUM_PROD_INVALIDOS_GEN;

public class PedidoGenerator extends ValueGenerator {

    PedidoBuilder pedidoBuilder = new PedidoBuilder();

    public Validation<Seq<String>, Pedido> pedidoGenerator() {
        NoDomainPedido noDomainPedido = pedidoGen();
        int numProd = randomNum(1, 11);
        List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productos =
            productosGen(numProd);
        Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedido = valorPedidoOf(productos);

        return pedidoBuilder.build(
            noDomainPedido.getId(),
            noDomainPedido.getEstado(),
            ISODateTimeFormat.dateTimeParser().parseDateTime(noDomainPedido.getFechaCreacion()),
            noDomainPedido.getCompradorId(),
            noDomainPedido.getDireccion(),
            productos,
            valorPedido);
    }

    public Validation<Seq<String>, Pedido> pedidoInvalidFGenerator() {
        NoDomainPedido noDomainPedido = pedidoInvalidFGen();
        int numProd = randomNum(1, 11);
        List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productos =
            productosGen(numProd);
        Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedido = valorPedidoOf(productos);

        return pedidoBuilder.build(
            noDomainPedido.getId(),
            noDomainPedido.getEstado(),
            ISODateTimeFormat.dateTimeParser().parseDateTime(noDomainPedido.getFechaCreacion()),
            noDomainPedido.getCompradorId(),
            noDomainPedido.getDireccion(),
            productos,
            valorPedido);
    }

    public Validation<Seq<String>, Pedido> pedidoFValidAndProdInvalidFGenerator() {
        NoDomainPedido noDomainPedido = pedidoGen();
        List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productosInvF =
            productosInvalidFGen(NUM_PROD_INVALIDOS_GEN);
        Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedido = valorPedidoGen();
        return pedidoBuilder.build(
            noDomainPedido.getId(),
            noDomainPedido.getEstado(),
            ISODateTimeFormat.dateTimeParser().parseDateTime(noDomainPedido.getFechaCreacion()),
            noDomainPedido.getCompradorId(),
            noDomainPedido.getDireccion(),
            productosInvF,
            valorPedido);
    }

    public Validation<Seq<String>, Pedido> pedidoFValidAndValorPedidoInvalidFGenerator() {
        NoDomainPedido noDomainPedido = pedidoGen();
        int numProd = randomNum(1, 11);
        List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productos =
            productosGen(numProd);
        Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedidoInvF = valorPedidoIvalidFGen();
        return pedidoBuilder.build(
            noDomainPedido.getId(),
            noDomainPedido.getEstado(),
            ISODateTimeFormat.dateTimeParser().parseDateTime(noDomainPedido.getFechaCreacion()),
            noDomainPedido.getCompradorId(),
            noDomainPedido.getDireccion(),
            productos,
            valorPedidoInvF);
    }



    private Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>>
    valorPedidoOf(List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productos) {

        Tuple3<Long, Long, Long> total_subTotal_totalIva = productos
            .map(p ->
                Tuple.of(
                    p._4 * p._5,  // Cantidad * PrecioUnidad
                    p._4 * p._6._2,     // Cantidad * ValorBase
                    p._4 * p._6._3       // Cantidad * ValorIva
                )
            ).foldLeft(Tuple.of(0L, 0L, 0L),
                (p1, p2) -> Tuple.of(p1._1 + p2._1, p1._2 + p2._2, p1._3 + p2._3));

        List<Tuple3<Integer, Long, Long>> detalleIva = productos
            .map(p ->
                Tuple.of(p._4, p._6) // cantidad, iva
            )
            .groupBy(cant_iva -> cant_iva._2._1) // Group by tarifaIVa
            .map(m -> // mapa ( tarifaIva, List( cantid, iva(ti,vb,vi) ) )
                m._2.foldLeft(Tuple.of(0, 0L, 0L), // List( cantid, iva(ti,vb,vi)
                    (vL1, vL2) -> Tuple.of(
                        vL2._2._1,          // tarifaIva
                        vL1._2 + vL2._2._2 * vL2._1,// valorBase * cantidad de prod
                        vL1._3 + vL2._2._3 * vL2._1  // valorIva * cantidad de prod
                    )
                )
            ).toList();

        Long total = total_subTotal_totalIva._1;
        Long subTotal = total_subTotal_totalIva._2;
        Long totalIva = total_subTotal_totalIva._3;

        return Tuple.of(total, subTotal, totalIva, detalleIva);
    }
}
