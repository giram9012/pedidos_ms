package com.denkening.pedidos.domain.errors;

public class ProductoError extends DomainError {
    public ProductoError(String msg) {
        super(msg);
    }

    public ProductoError() {
        super("El precio del producto y el iva son inconsistentes");
    }
}
