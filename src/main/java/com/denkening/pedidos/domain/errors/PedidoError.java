package com.denkening.pedidos.domain.errors;

public class PedidoError extends DomainError {


    public PedidoError(String msg) {
        super(msg);
    }

    public PedidoError() {
        super("Existen inconsistencias en los productos y el  valor del pedido");
    }
}
