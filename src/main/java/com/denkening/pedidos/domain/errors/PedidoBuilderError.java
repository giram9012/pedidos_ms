package com.denkening.pedidos.domain.errors;

public class PedidoBuilderError extends DomainError {

    public PedidoBuilderError() {
        super("Verifique los productos y el valor del pedido");
    }

    public PedidoBuilderError(String msg) {
        super(msg);
    }
}
