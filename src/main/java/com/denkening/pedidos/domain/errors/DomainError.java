package com.denkening.pedidos.domain.errors;

public class DomainError extends RuntimeException {
    public DomainError(String msg) {
        super(msg);
    }
}
