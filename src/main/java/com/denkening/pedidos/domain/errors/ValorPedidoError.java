package com.denkening.pedidos.domain.errors;

public class ValorPedidoError extends  DomainError {
    public ValorPedidoError(String msg) {
        super(msg);
    }

    public ValorPedidoError() {
        super("Los valores en el valorPedido tienen insonsistencias");
    }
}
