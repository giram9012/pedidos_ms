package com.denkening.pedidos.domain.errors;

public class IvaError extends DomainError {

    public IvaError() {
        super("Los valores para el iva est\u00e1n mal calculados");
    }

    public IvaError(String msg) {
        super(msg);
    }
}
