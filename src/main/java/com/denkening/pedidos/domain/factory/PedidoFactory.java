package com.denkening.pedidos.domain.factory;

import com.denkening.pedidos.domain.model.pedido.Pedido;
import com.denkening.pedidos.domain.model.pedido.PedidoBuilder;
import io.vavr.Tuple3;
import io.vavr.Tuple4;
import io.vavr.Tuple6;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import org.joda.time.DateTime;

public interface PedidoFactory<T> {

    Validation<Seq<String>, Pedido> of(T pedido);

    default Validation<Seq<String>, Pedido> of(String uuid, String estado, DateTime fechaCreacion,
                                               String compradorId, String direccion,
                                               List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productos,
                                               Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedido) {
        PedidoBuilder pedidoV = new PedidoBuilder();
        return pedidoV.build(uuid, estado, fechaCreacion, compradorId, direccion, productos, valorPedido);
    }
}
