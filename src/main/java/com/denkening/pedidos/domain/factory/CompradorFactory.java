package com.denkening.pedidos.domain.factory;

import com.denkening.pedidos.domain.model.comprador.Comprador;
import com.denkening.pedidos.domain.model.comprador.CompradorBuilder;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;

public interface CompradorFactory<T> {

    default Validation<Seq<String>, Comprador> of(String id, String nombre, String telefono) {

        CompradorBuilder builder = new CompradorBuilder();
        return builder.build(id, nombre, telefono);
    }

    Comprador of(T comprador);
}
