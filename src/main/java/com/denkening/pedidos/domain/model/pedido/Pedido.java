package com.denkening.pedidos.domain.model.pedido;

import com.denkening.pedidos.domain.model.estadopedido.Estado;
import com.denkening.pedidos.domain.model.producto.Producto;
import com.denkening.pedidos.domain.model.valorpedido.ValorPedido;
import io.vavr.collection.List;
import org.joda.time.DateTime;

import java.util.UUID;

public class Pedido {

    private UUID id;
    private Estado estado;
    private DateTime fechaCreacion;
    private UUID compradorId;
    private String direccion;
    private List<Producto> productos;
    private ValorPedido valorPedido;

    public Pedido(UUID id, Estado estado, DateTime fechaCreacion, UUID compradorId,
           String direccion, List<Producto> productos, ValorPedido valorPedido) {
        this.id = id;
        this.estado = estado;
        this.fechaCreacion = fechaCreacion;
        this.compradorId = compradorId;
        this.direccion = direccion;
        this.productos = productos;
        this.valorPedido = valorPedido;
    }

    public UUID getId() {
        return id;
    }

    public Estado getEstado() {
        return estado;
    }

    public DateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public UUID getCompradorId() {
        return compradorId;
    }

    public String getDireccion() {
        return direccion;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public ValorPedido getValorPedido() {
        return valorPedido;
    }

    @Override
    public String toString() {
        return
            "Pedido{" +
            "id=" + id +
            ", estado=" + estado +
            ", fechaCreacion=" + fechaCreacion +
            ", compradorId=" + compradorId +
            ", direccion='" + direccion + '\'' +
            ", productos=" + productos +
            ", valorPedido=" + valorPedido +
            '}';
    }
}
