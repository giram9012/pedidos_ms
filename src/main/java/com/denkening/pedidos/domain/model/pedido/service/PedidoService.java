package com.denkening.pedidos.domain.model.pedido.service;

import com.denkening.pedidos.domain.model.estadopedido.Estado;
import com.denkening.pedidos.domain.model.pedido.Pedido;
import com.denkening.pedidos.domain.model.pedido.repository.PedidoRepository;

public class PedidoService {

    private final PedidoRepository repo;

    public PedidoService(PedidoRepository repo) {
        this.repo = repo;
    }

    public Pedido hacerPedido(Pedido pedido) {
        return repo.save(pedido);
    }

    public Pedido cambiarEstado(String id, Estado estado){
        return repo.update(id, estado);
    }

}
