package com.denkening.pedidos.domain.model.estadopedido;

public class Confirmado implements Estado {

    @Override
    public String value() {
        return "CONFIRMADO";
    }
}
