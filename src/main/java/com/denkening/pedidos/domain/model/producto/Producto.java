package com.denkening.pedidos.domain.model.producto;

import com.denkening.pedidos.domain.model.iva.Iva;
import com.denkening.pedidos.domain.errors.ProductoError;
import com.denkening.pedidos.domain.model.tarifaiva.TarifaIva;

import java.util.UUID;

public class Producto {

    private UUID id;
    private String nombre;
    private String descripcion;
    private int cantidad;
    private Long precioUnidad;
    private Iva iva;

    public Producto(UUID id, String nombre, String descripcion, int cantidad, Long precioUnidad, Iva iva) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.precioUnidad = precioUnidad;
        this.iva = iva;
    }


    public UUID getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public Long getPrecioUnidad() {
        return precioUnidad;
    }

    public Iva getIva() {
        return iva;
    }

    public TarifaIva getTarifaIva() {
        return iva.getTarifaIva();
    }

    public Long getValorBase() {

        return iva.getValorBase();
    }

    public Long getValorIva() {

        return iva.getValorIva();
    }
}
