package com.denkening.pedidos.domain.model.producto;

import com.denkening.pedidos.domain.model.CommonsValidators;
import com.denkening.pedidos.domain.model.iva.Iva;
import com.denkening.pedidos.domain.model.iva.IvaBuilder;
import io.vavr.Tuple3;
import io.vavr.Tuple6;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;

public class ProductoBuilder extends CommonsValidators {

    public static final String VALID_NOMBRE_CHARS = "[0-9a-zA-Z ]{2,}";
    public static final String VALID_DESCRIPCION_CHARS = "[0-9a-zA-Z ]{3,}";
    public static final int MIN_CANTIDAD = 1;

    public Validation<Seq<String>, List<Producto>> build(List<Tuple6<String, String, String, Integer, Long,
        Tuple3<Integer, Long, Long>>> productos) {

        IvaBuilder ivaV = new IvaBuilder();

        List<Validation<Seq<String>, Producto>> vProdsL = productos.map(
            p -> {
                Validation<Seq<String>, Iva> vIva = ivaV.build(p._6._1, p._6._2, p._6._3);
                Iva iva = vIva.isValid() ? vIva.get() : new Iva();
                Seq<String> erroresIva = vIva.isValid() ? List.empty() : vIva.getError();
                Validation<Seq<String>, Producto> producto = validateProducto(p._1, p._2, p._3, p._4, p._5, iva);
                return producto.isValid() ? producto : Validation.invalid(erroresIva.appendAll(producto.getError()));
            }
        );

        List<Producto> productosL = vProdsL.filter(Validation::isValid).map(Validation::get);
        List<String> erroresProductos = vProdsL.filter(Validation::isInvalid).flatMap(Validation::getError);

        return erroresProductos.isEmpty() ? Validation.valid(productosL) :
            Validation.invalid(erroresProductos);
    }

    private Validation<Seq<String>, Producto> validateProducto(String uuid, String nombre, String descripcion,
                                                               int cantidad, Long precioUnidad, Iva iva) {

        Validation<Seq<String>, Producto> producto = Validation.combine(validateUUID(uuid), validateNombre(nombre),
            validateDescripcion(descripcion), validateCantidad(cantidad), validateDinero(precioUnidad),
            Validation.valid(iva))
            .ap(Producto::new);

        if (producto.isValid())
            return validProducto(precioUnidad, iva) ?
                producto : Validation.invalid(List.of("El precio del producto y el iva son inconsistentes"));
        else
            return producto;
    }

    private Validation<String, String> validateNombre(String nombre) {
        return validateRegExp(VALID_NOMBRE_CHARS, nombre) ? Validation.valid(nombre)
            :  Validation.valid(nombre); // Validation.invalid("Nombre del producto no es valido: " + nombre);
    }

    private Validation<String, String> validateDescripcion(String descripcion) {
        return validateRegExp(VALID_DESCRIPCION_CHARS, descripcion) ? Validation.valid(descripcion)
            : Validation.valid(descripcion); //Validation.invalid("Descripcion del producto no es valido: " + descripcion);

    }

    private Validation<String, Integer> validateCantidad(int cantidad) {
        return cantidad < MIN_CANTIDAD
            ? Validation.invalid("Cantidad debe ser minimo " + MIN_CANTIDAD)
            : Validation.valid(cantidad);
    }

    private boolean validProducto(Long precioUnidad, Iva iva) {
        if (precioUnidad == null || iva.getValorBase() == null || iva.getValorIva() == null)
            return false;
        return precioUnidad.equals(iva.getValorBase() + iva.getValorIva());
    }
}
