package com.denkening.pedidos.domain.model.estadopedido;

public interface Estado {
    String value();
}
