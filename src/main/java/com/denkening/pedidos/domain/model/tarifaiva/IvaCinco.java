package com.denkening.pedidos.domain.model.tarifaiva;

public class IvaCinco implements TarifaIva {

    @Override
    public int value() {
        return 5;
    }
}