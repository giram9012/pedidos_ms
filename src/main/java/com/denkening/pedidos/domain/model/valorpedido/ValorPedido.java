package com.denkening.pedidos.domain.model.valorpedido;


import com.denkening.pedidos.domain.model.iva.Iva;
import io.vavr.collection.List;

public class ValorPedido {

    private Long total;
    private Long subTotal;
    private Long totalIva;
    private List<Iva> detalleIva;

    public ValorPedido() {
    }

    public ValorPedido(Long total, Long subTotal, Long totalIva, List<Iva> detalleIva) {
        this.total = total;
        this.subTotal = subTotal;
        this.totalIva = totalIva;
        this.detalleIva = detalleIva;
    }

    public Long getTotal() {
        return total;
    }

    public Long getSubTotal() {
        return subTotal;
    }

    public Long getTotalIva() {
        return totalIva;
    }

    public List<Iva> getDetalleIva() {
        return detalleIva;
    }
}
