package com.denkening.pedidos.domain.model.tarifaiva;

public interface TarifaIva {
    int value();
}