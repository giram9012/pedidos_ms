package com.denkening.pedidos.domain.model.estadopedido;

public class Anulado implements Estado {

    @Override
    public String value() {
        return "ANULADO";
    }
}


