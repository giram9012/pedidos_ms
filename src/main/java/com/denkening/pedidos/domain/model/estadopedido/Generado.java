package com.denkening.pedidos.domain.model.estadopedido;

public class Generado implements Estado {

    @Override
    public String value() {
        return "GENERADO";
    }
}
