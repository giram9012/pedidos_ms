package com.denkening.pedidos.domain.model.iva;

import com.denkening.pedidos.domain.model.CommonsValidators;
import com.denkening.pedidos.domain.model.tarifaiva.IvaCero;
import com.denkening.pedidos.domain.model.tarifaiva.IvaCinco;
import com.denkening.pedidos.domain.model.tarifaiva.IvaDiecinueve;
import com.denkening.pedidos.domain.model.tarifaiva.TarifaIva;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;

import static io.vavr.API.*;

public class IvaBuilder extends CommonsValidators {

    public Validation<Seq<String>, Iva> build(int tarifaIva, Long valorBase, Long valorIva) {

        Validation<Seq<String>, Iva> iva = Validation.combine(validateTarifaIva(tarifaIva), validateDinero(valorBase),
            validateDinero(valorIva)).ap(Iva::new);
        if (iva.isValid())
            return (validIva(tarifaIva, valorBase, valorIva)) ? iva :
                Validation.invalid(List.of("Los valores para el iva est\u00e1n mal calculados"));
        else
            return iva;
    }

    private Validation<String, TarifaIva> validateTarifaIva(int tarifaIva) {
        return Match(tarifaIva).of(
            Case($(0), Validation.valid(new IvaCero())),
            Case($(5), Validation.valid(new IvaCinco())),
            Case($(19), Validation.valid(new IvaDiecinueve())),
            Case($(), Validation.invalid("Tarifa Iva no valida : " + tarifaIva)));
    }

    private boolean validIva(int tarifaIva, Long valorBase, Long valorIva) {
        if(valorBase == null || valorIva == null)
            return false;

        return Math.round(Double.valueOf(valorIva) / Double.valueOf(valorBase) * 100.0) == tarifaIva;
    }
}
