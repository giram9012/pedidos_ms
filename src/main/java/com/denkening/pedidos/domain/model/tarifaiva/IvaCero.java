package com.denkening.pedidos.domain.model.tarifaiva;

public class IvaCero implements TarifaIva {

    @Override
    public int value() {
        return 0;
    }
}
