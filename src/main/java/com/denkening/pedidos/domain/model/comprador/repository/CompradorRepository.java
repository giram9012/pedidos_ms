package com.denkening.pedidos.domain.model.comprador.repository;

import com.denkening.pedidos.domain.model.comprador.Comprador;

import java.util.Optional;

public interface CompradorRepository {

    Comprador save(Comprador comprador);
}
