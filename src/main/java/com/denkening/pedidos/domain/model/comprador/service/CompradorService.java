package com.denkening.pedidos.domain.model.comprador.service;

import com.denkening.pedidos.domain.model.comprador.Comprador;
import com.denkening.pedidos.domain.model.comprador.repository.CompradorRepository;

import java.util.Optional;

public class CompradorService {

    private final CompradorRepository repo;

    public CompradorService(CompradorRepository repo) {
        this.repo = repo;
    }

    public Comprador crear(Comprador comprador) {
        return repo.save(comprador);
    }
}
