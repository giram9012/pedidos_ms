package com.denkening.pedidos.domain.model.comprador;

import com.denkening.pedidos.domain.model.CommonsValidators;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;

public class CompradorBuilder extends CommonsValidators {

    public static final String VALID_NOMBRE_CHARS = "[a-zA-Z ]{2,}";
    public static final String VALID_TELEFONO_CHARS = "[0-9]{7}|[3]{1}[0-9]{9}";

    public Validation<Seq<String>, Comprador> build(String uuid, String nombre, String telefono) {
        return Validation
            .combine(validateUUID(uuid), validateNombre(nombre), validateTelefono(telefono))
            .ap(Comprador::new);
    }

    private Validation<String, String> validateNombre(String nombre) {
        return validateRegExp(VALID_NOMBRE_CHARS, nombre) ? Validation.valid(nombre)
            : Validation.invalid("Nombre del comprador no es valido: "+ nombre);
    }

    private Validation<String, String> validateTelefono(String telefono) {
        return validateRegExp(VALID_TELEFONO_CHARS, telefono) ? Validation.valid(telefono)
            : Validation.invalid("Telefono del comprador no es valido: "+ telefono);
    }

}
