package com.denkening.pedidos.domain.model.tarifaiva;

public class IvaDiecinueve implements TarifaIva {

    @Override
    public int value() {
        return 19;
    }
}