package com.denkening.pedidos.domain.model.valorpedido;

import com.denkening.pedidos.domain.model.CommonsValidators;
import com.denkening.pedidos.domain.model.iva.Iva;
import com.denkening.pedidos.domain.model.iva.IvaBuilder;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.Tuple3;
import io.vavr.Tuple4;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;

public class ValorPedidoBuilder extends CommonsValidators {

    public Validation<Seq<String>, ValorPedido> build(Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedido) {
        IvaBuilder ivaBuilder = new IvaBuilder();

        List<Validation<Seq<String>, Iva>> vDetalleIva = valorPedido._4.map(
            dIva -> ivaBuilder.build(dIva._1, dIva._2, dIva._3));

        List<Iva> detalleIva = vDetalleIva.filter(Validation::isValid).map(Validation::get);
        List<String> erroresDetalleIva = vDetalleIva.filter(Validation::isInvalid).flatMap(Validation::getError);

        Validation<Seq<String>, ValorPedido> vValorPed =
            validatePedido(valorPedido._1, valorPedido._2, valorPedido._3, detalleIva);

        return vValorPed.isValid() ?
            vValorPed : Validation.invalid(erroresDetalleIva.appendAll(vValorPed.getError()));
    }

    private Validation<Seq<String>, ValorPedido> validatePedido(Long total, Long subTotal, Long totalIva,
                                                                List<Iva> detalleIva) {
        Validation<Seq<String>, ValorPedido> valorpedido = Validation.combine(validateDinero(total),
            validateDinero(subTotal), validateDinero(totalIva), Validation.valid(detalleIva)).ap(ValorPedido::new);

        if (valorpedido.isValid())
            return validValorPedido(total, subTotal, totalIva, detalleIva) ? valorpedido :
                Validation.invalid(List.of("Los valores en el valorPedido tienen insonsistencias"));
        else
            return valorpedido;
    }

    private boolean validValorPedido(Long total, Long subTotal, Long totalIva, List<Iva> detalleIva) {
        if (detalleIva.isEmpty())
            return false;

        Tuple2<Long, Long> subTotal_totalIva = detalleIva
            .foldLeft(Tuple.of(0L, 0L),
                (acc, iva) -> Tuple.of(acc._1 + iva.getValorBase(), acc._2 + iva.getValorIva()));

        return subTotal.equals(subTotal_totalIva._1) && totalIva.equals(subTotal_totalIva._2) &&
            total.equals(subTotal_totalIva._1 + subTotal_totalIva._2);
    }
}
