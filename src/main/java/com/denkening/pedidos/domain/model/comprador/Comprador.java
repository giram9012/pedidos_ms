package com.denkening.pedidos.domain.model.comprador;

import java.util.UUID;

public class Comprador {

    private UUID id;
    private String nombre;
    private String telefono;

    public Comprador(UUID id, String nombre, String telefono) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
    }

    public UUID getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    @Override
    public String toString() {
        return "Comprador{" +
            "id=" + id +
            ", nombre='" + nombre + '\'' +
            ", telefono='" + telefono + '\'' +
            '}';
    }
}
