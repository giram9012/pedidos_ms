package com.denkening.pedidos.domain.model.pedido.repository;

import com.denkening.pedidos.domain.model.estadopedido.Estado;
import com.denkening.pedidos.domain.model.pedido.Pedido;

import java.util.Optional;

public interface PedidoRepository {

    Pedido save(Pedido pedido);

    Pedido update(String id, Estado estado);
}
