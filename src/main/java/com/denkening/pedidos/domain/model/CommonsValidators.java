package com.denkening.pedidos.domain.model;

import io.vavr.control.Validation;

import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonsValidators {

    protected Validation<String, UUID> validateUUID(String uuid) {
        try {
            return Validation.valid(UUID.fromString(uuid));
        } catch (IllegalArgumentException e) {
            return Validation.invalid(e.getMessage());
        }
    }

    protected Validation<String, Long> validateDinero(Long dinero) {
        return dinero < 0
            ? Validation.invalid("Valor de dinero no valido : "+ dinero.toString())
            : Validation.valid(dinero);
    }

    protected Validation<String, String> validateNonEmptyString(String str, String errorFieldMs) {
        return str.isEmpty() ? Validation.invalid(errorFieldMs +" vacia")
            : Validation.valid(str);
    }

    protected boolean validateRegExp(String pattern, String match) {
        Pattern pat = Pattern.compile(pattern);
        Matcher mat = pat.matcher(match);
        return mat.matches();
    }

}
