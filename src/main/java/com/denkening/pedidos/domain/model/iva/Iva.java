
package com.denkening.pedidos.domain.model.iva;

import com.denkening.pedidos.domain.model.tarifaiva.TarifaIva;

public class Iva {

    private TarifaIva tarifaIva;
    private Long valorBase;
    private Long valorIva;

    public Iva() {
    }

    public Iva(TarifaIva tarifaIva, Long valorBase, Long valorIva) {
        this.tarifaIva = tarifaIva;
        this.valorBase = valorBase;
        this.valorIva = valorIva;
    }

    public TarifaIva getTarifaIva() {
        return tarifaIva;
    }

    public Long getValorBase() {
        return valorBase;
    }

    public Long getValorIva() {
        return valorIva;
    }
}
