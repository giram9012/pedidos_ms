package com.denkening.pedidos.domain.model.pedido;

import com.denkening.pedidos.domain.model.CommonsValidators;
import com.denkening.pedidos.domain.model.estadopedido.Anulado;
import com.denkening.pedidos.domain.model.estadopedido.Confirmado;
import com.denkening.pedidos.domain.model.estadopedido.Estado;
import com.denkening.pedidos.domain.model.estadopedido.Generado;
import com.denkening.pedidos.domain.model.producto.Producto;
import com.denkening.pedidos.domain.model.producto.ProductoBuilder;
import com.denkening.pedidos.domain.model.valorpedido.ValorPedido;
import com.denkening.pedidos.domain.model.valorpedido.ValorPedidoBuilder;
import io.vavr.Tuple;
import io.vavr.Tuple3;
import io.vavr.Tuple4;
import io.vavr.Tuple6;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import static io.vavr.API.*;

public class PedidoBuilder extends CommonsValidators {


    public Validation<Seq<String>, Pedido> build(String uuid, String estado, DateTime fechaCreacion,
                                                 String compradorId, String direccion,
                                                 List<Tuple6<String, String, String, Integer, Long, Tuple3<Integer, Long, Long>>> productos,
                                                 Tuple4<Long, Long, Long, List<Tuple3<Integer, Long, Long>>> valorPedido
    ) {

        ProductoBuilder productoBuilder = new ProductoBuilder();
        ValorPedidoBuilder valorPedidoBuilder = new ValorPedidoBuilder();

        Validation<Seq<String>, List<Producto>> vProds = productoBuilder.build(productos);
        List<Producto> productosL = vProds.isValid() ? vProds.get() : List.empty();
        Seq<String> erroresProds = vProds.isValid() ? List.empty() : vProds.getError();

        Validation<Seq<String>, ValorPedido> vValorPedido = valorPedidoBuilder.build(valorPedido);
        ValorPedido valorPedidoT = vValorPedido.isValid() ? vValorPedido.get() : new ValorPedido();
        Seq<String> erroresValorPedido = vValorPedido.isValid() ? List.empty() : vValorPedido.getError();

        Validation<Seq<String>, Pedido> pedido = validateFieldsPedido(uuid, estado, fechaCreacion, compradorId, direccion,
            productosL, valorPedidoT);

        if (pedido.isValid())
            return validProducto(productosL, valorPedidoT) ? pedido :
                Validation.invalid(erroresValorPedido
                    .appendAll(
                        erroresProds.append("\"Existen inconsistencias en los productos y el  valor del pedido\"")
                    )
                );
        else
            return Validation.invalid(erroresValorPedido.appendAll(erroresProds.appendAll(pedido.getError())));
    }

    private Validation<Seq<String>, Pedido> validateFieldsPedido(String uuid, String estado, DateTime fechaCreacion,
                                                                 String compradorId, String direccion,
                                                                 List<Producto> productos, ValorPedido valorPedido) {

        return Validation.combine(validateUUID(uuid), validateEstado(estado), validateFechaCreacion(fechaCreacion),
            validateUUID(compradorId), validateNonEmptyString(direccion, "Direccion"),
            Validation.valid(productos), Validation.valid(valorPedido))
            .ap(Pedido::new);
    }

    private Validation<String, DateTime> validateFechaCreacion(DateTime fechaCreacion) {
        DateTime hoy = DateTime.now().withZone(DateTimeZone.UTC);
        return fechaCreacion.isBefore(hoy) ? Validation.valid(fechaCreacion) :
            Validation.invalid("Fecha de creación del pedido no valida : ");
    }

    public Validation<String, Estado> validateEstado(String estado) {
        return Match(estado).of(
            Case($("GENERADO"), Validation.valid(new Generado())),
            Case($("CONFIRMADO"), Validation.valid(new Confirmado())),
            Case($("ANULADO"), Validation.valid(new Anulado())),
            Case($(), Validation.invalid("Estado de pedido no valida : " + estado)));
    }

    private boolean validProducto(List<Producto> productos, ValorPedido valorPedido) {

        if (productos.isEmpty() || valorPedido == null)
            return false;

        Tuple3<Long, Long, Long> totalProd_TotalBase_TotalIva = productos
            .map(p -> {
                int cantidad = p.getCantidad();
                return Tuple.of(cantidad * p.getPrecioUnidad(), cantidad * p.getValorBase(),
                    cantidad * p.getValorIva());
            })
            .foldLeft(
                Tuple.of(0L, 0L, 0L),
                (acc, valoresTotalesXProd) -> (
                    Tuple.of(
                        acc._1 + valoresTotalesXProd._1,
                        acc._2 + valoresTotalesXProd._2,
                        acc._3 + valoresTotalesXProd._3
                    )));

        Long totalProds = totalProd_TotalBase_TotalIva._1;
        Long totalBaseProds = totalProd_TotalBase_TotalIva._2;
        Long totalIvaProds = totalProd_TotalBase_TotalIva._3;

        return totalProds.equals(valorPedido.getTotal()) && totalBaseProds.equals(valorPedido.getSubTotal()) &&
            totalIvaProds.equals(valorPedido.getTotalIva());
    }
}
