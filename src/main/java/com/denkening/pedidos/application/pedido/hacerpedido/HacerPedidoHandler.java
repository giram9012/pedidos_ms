package com.denkening.pedidos.application.pedido.hacerpedido;

import com.denkening.pedidos.domain.factory.PedidoFactory;
import com.denkening.pedidos.domain.model.pedido.Pedido;
import com.denkening.pedidos.domain.model.pedido.service.PedidoService;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class HacerPedidoHandler {

    private final PedidoService pedidoService;
    private final PedidoFactory pedidoFactory;

    @Autowired
    public HacerPedidoHandler(PedidoService pedidoService,PedidoFactory pedidoFactory) {
        this.pedidoService = pedidoService;
        this.pedidoFactory = pedidoFactory;
    }

    @Transactional
    public Pedido execute(HacerPedidoCommand command) {
        Validation<Seq<String>, Pedido> pedidoV = pedidoFactory.of(command);
        if (pedidoV.isValid())
            return pedidoService.hacerPedido(pedidoV.get());
        throw new RuntimeException("return all messages of validation"); // Todo
    }
}
