package com.denkening.pedidos.application.pedido.hacerpedido.command;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class IvaCommand {
    private int tarifaIva;
    private Long valorBase;
    private Long valorIva;
}

