package com.denkening.pedidos.application.pedido.hacerpedido.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ValorPedidoCommand {
    private Long total;
    private Long subTotal;
    private Long totalIva;
    private List<IvaCommand> detalleIva;
}
