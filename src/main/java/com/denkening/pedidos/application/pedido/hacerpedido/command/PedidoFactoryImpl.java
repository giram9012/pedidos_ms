package com.denkening.pedidos.application.pedido.hacerpedido.command;

import com.denkening.pedidos.application.pedido.hacerpedido.HacerPedidoCommand;
import com.denkening.pedidos.domain.factory.PedidoFactory;
import com.denkening.pedidos.domain.model.estadopedido.Generado;
import com.denkening.pedidos.domain.model.pedido.Pedido;
import io.vavr.Tuple;
import io.vavr.Tuple4;
import io.vavr.collection.List;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import org.joda.time.format.ISODateTimeFormat;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.stream.Collectors;

@Component
public class PedidoFactoryImpl implements PedidoFactory<HacerPedidoCommand> {

    @Override
    public Validation<Seq<String>, Pedido> of(HacerPedidoCommand command) {

        List productos = List.of(command.getProductos())
            .flatMap(noDomainProducts ->
                noDomainProducts.stream().map(
                    p -> Tuple.of(p.getId(), p.getNombre(), p.getDescripcion(), p.getCantidad(), p.getPrecioUnidad()
                        , Tuple.of(p.getIva().getTarifaIva(), p.getIva().getValorBase(), p.getIva().getValorIva()))
                ).collect(Collectors.toList())
            );

        List detalleIva = List.of(command.getValorPedido().getDetalleIva())
            .flatMap(noDomainIvas ->
                noDomainIvas.stream().map(
                    di -> Tuple.of(di.getTarifaIva(), di.getValorBase(), di.getValorIva())
                ).collect(Collectors.toList())
            );

        Tuple4 valorPedido = Tuple.of(command.getValorPedido().getTotal(), command.getValorPedido().getSubTotal(),
            command.getValorPedido().getTotalIva(), detalleIva);


        return of(
            UUID.randomUUID().toString(),
            new Generado().value(),
            ISODateTimeFormat.dateTimeParser().parseDateTime(command.getFechaCreacion()),
            command.getCompradorId(),
            command.getDireccion(),
            productos,
            valorPedido
        );
    }
}
