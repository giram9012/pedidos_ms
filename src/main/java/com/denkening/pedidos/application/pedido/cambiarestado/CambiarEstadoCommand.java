package com.denkening.pedidos.application.pedido.cambiarestado;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CambiarEstadoCommand {
    private String id;
    private String estado;
}
