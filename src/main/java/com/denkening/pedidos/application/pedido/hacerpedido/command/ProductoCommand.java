package com.denkening.pedidos.application.pedido.hacerpedido.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductoCommand {
    private String id;
    private String nombre;
    private String descripcion;
    private int cantidad;
    private Long precioUnidad;
    private IvaCommand iva;
}