package com.denkening.pedidos.application.pedido.hacerpedido;

import com.denkening.pedidos.application.pedido.hacerpedido.command.ProductoCommand;
import com.denkening.pedidos.application.pedido.hacerpedido.command.ValorPedidoCommand;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class HacerPedidoCommand {
    private String fechaCreacion;
    private String compradorId;
    private String direccion;
    private List<ProductoCommand> productos;
    private ValorPedidoCommand valorPedido;
}
