package com.denkening.pedidos.application.pedido.cambiarestado;

import com.denkening.pedidos.domain.model.estadopedido.Anulado;
import com.denkening.pedidos.domain.model.estadopedido.Confirmado;
import com.denkening.pedidos.domain.model.estadopedido.Estado;
import com.denkening.pedidos.domain.model.estadopedido.Generado;
import com.denkening.pedidos.domain.model.pedido.Pedido;
import com.denkening.pedidos.domain.model.pedido.service.PedidoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class CambiarEstadoHandler {

    private final PedidoService pedidoService;

    @Autowired
    public CambiarEstadoHandler(PedidoService pedidoService) {
        this.pedidoService = pedidoService;
    }

    @Transactional
    public Pedido execute(CambiarEstadoCommand command) {
        Estado newEstado = validateEstado(command.getEstado());
        return pedidoService.cambiarEstado(command.getId(), newEstado);
    }

    private Estado validateEstado(String estadoC) {
        switch (estadoC) {
            case "GENERADO":
                return new Generado();
            case "CONFIRMADO":
                return new Confirmado();
            case "ANULADO":
                return new Anulado();
            default:
                throw new RuntimeException("No es posible actualizar el pedido a el estado" + estadoC); // TODO
        }
    }

}
