package com.denkening.pedidos.application.comprador.crear;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CrearCompradorCommand {
    private String nombre;
    private String telefono;
}
