package com.denkening.pedidos.application.comprador.crear;

import com.denkening.pedidos.domain.factory.CompradorFactory;
import com.denkening.pedidos.domain.model.comprador.Comprador;
import com.denkening.pedidos.domain.model.comprador.service.CompradorService;
import io.vavr.collection.Seq;
import io.vavr.control.Validation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CompradorFactoryImpl implements CompradorFactory<CrearCompradorCommand> {

    private final CompradorService compradorService;

    @Autowired
    public CompradorFactoryImpl(CompradorService compradorService) {
        this.compradorService = compradorService;
    }

    @Override
    public Comprador of(CrearCompradorCommand c) {
        Validation<Seq<String>, Comprador> comprador = of(UUID.randomUUID().toString(), c.getNombre(), c.getTelefono());
        if (comprador.isValid())
            return compradorService.crear(comprador.get());

        throw new RuntimeException("No se pudo crear el comprador y devolver todos los errores encontrados");
    }
}
