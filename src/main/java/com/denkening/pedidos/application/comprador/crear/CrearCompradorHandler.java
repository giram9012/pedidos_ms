package com.denkening.pedidos.application.comprador.crear;

import com.denkening.pedidos.domain.factory.CompradorFactory;
import com.denkening.pedidos.domain.model.comprador.Comprador;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CrearCompradorHandler {

    private CompradorFactory compradorFactory;

    @Autowired
    public CrearCompradorHandler(CompradorFactory compradorFactory) {
        this.compradorFactory = compradorFactory;
    }

    public Comprador execute(CrearCompradorCommand command) {
        return compradorFactory.of(command);
    }
}
