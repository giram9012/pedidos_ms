package com.denkening.pedidos.infrastructure.web.controller.pedido;

import com.denkening.pedidos.infrastructure.database.producto.ProductoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductoDTO {
    private String id;
    private String nombre;
    private String descripcion;
    private int cantidad;
    private Long precioUnidad;
    private IvaDTO iva;

    public static final List<ProductoDTO> fromEntity(List<ProductoEntity> productoE) {
        return productoE.stream().map(
            p -> new ProductoDTO(
                p.getProductId(),
                p.getNombre(),
                p.getDescripcion(),
                p.getCantidad(),
                p.getPrecioUnidad(),
                new IvaDTO(
                    p.getTarifaIva(),
                    p.getValorBase(),
                    p.getValorIva()
                )
            )
        ).collect(Collectors.toList());
    }
}
