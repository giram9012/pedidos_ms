package com.denkening.pedidos.infrastructure.web.controller.pedido;

import com.denkening.pedidos.domain.model.pedido.Pedido;
import com.denkening.pedidos.infrastructure.database.pedido.PedidoEntity;
import com.denkening.pedidos.infrastructure.web.controller.comprador.CompradorDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PedidoDTO {
    private String id;
    private String estado;
    private String fechaCreacion;
    private CompradorDTO comprador;
    private String direccion;
    private List<ProductoDTO> productos;
    private ValorPedidoDTO valorPedido;

    public final static PedidoDTO fromEntity(PedidoEntity pedidoE) {
        return new PedidoDTO(
            pedidoE.getId(),
            pedidoE.getEstado().getNombre(),
            pedidoE.getFechaCreacion().toString(),
            CompradorDTO.fromEntity(pedidoE.getComprador()),
            pedidoE.getDireccion(),
            ProductoDTO.fromEntity(pedidoE.getProductos()),
            ValorPedidoDTO.fromEntity(pedidoE.getValorPedido())
        );
    }
}