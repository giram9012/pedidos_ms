package com.denkening.pedidos.infrastructure.web.controller.comprador;

import com.denkening.pedidos.application.comprador.crear.CrearCompradorCommand;
import com.denkening.pedidos.application.comprador.crear.CrearCompradorHandler;
import com.denkening.pedidos.domain.model.comprador.Comprador;
import com.denkening.pedidos.domain.pagination.Paginated;
import com.denkening.pedidos.domain.pagination.Paginator;
import com.denkening.pedidos.infrastructure.database.comprador.CompradorEntity;
import com.denkening.pedidos.infrastructure.database.comprador.CompradorRepositorySQL;
import com.denkening.pedidos.infrastructure.database.pedido.PedidoEntity;
import com.denkening.pedidos.infrastructure.web.controller.pagination.RequestPaginator;
import com.denkening.pedidos.infrastructure.web.controller.pedido.PedidoDTO;
import com.denkening.pedidos.infrastructure.web.controller.response.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/compradores")
public class CompradorController {

    private final CrearCompradorHandler crearCompradorHandler;
    private final CompradorRepositorySQL repoSQL;

    @Autowired
    public CompradorController(CrearCompradorHandler crearCompradorHandler, CompradorRepositorySQL repoSQL) {
        this.crearCompradorHandler = crearCompradorHandler;
        this.repoSQL = repoSQL;
    }

    @GetMapping
    public ResponseEntity<?> buscarByTelefono(@RequestParam(name = "telefono") String telefono,
                                              @RequestParam(defaultValue = "0") int page,
                                              @RequestParam(defaultValue = "20") int size, HttpServletRequest request) {

        Paginated<CompradorEntity> paginatedResult = repoSQL.findAllEntitiesByTelefono(telefono,  page, size);
        Paginator paginator = RequestPaginator.of(paginatedResult.getPaginator(), request, size, page, "telefono="+telefono+"&");

        List<CompradorDTO> CompradoresDTO = paginatedResult.getContent().stream().map(c -> CompradorDTO.fromEntity(c)).collect(Collectors.toList());
        return new ApiResponse().status(HttpStatus.OK).content(CompradoresDTO).paginator(paginator).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> buscarBy(@PathVariable String id) {
        CompradorEntity compradorE = repoSQL.findEntityById(id).orElseThrow(
            () -> new EntityNotFoundException());
        return new ApiResponse()
            .status(HttpStatus.OK)
            .content(CompradorDTO.fromEntity(compradorE))
            .build();
    }

    @PostMapping
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> crear(@RequestBody CrearCompradorCommand command) {
        Comprador comprador = crearCompradorHandler.execute(command);
        return new ApiResponse().status(HttpStatus.CREATED).content(comprador).build();
    }


}


