package com.denkening.pedidos.infrastructure.web.controller.pagination;

import com.denkening.pedidos.domain.pagination.Paginator;

import javax.servlet.http.HttpServletRequest;

public interface RequestPaginator extends Paginator {
	String getPreviousPageUrl();
	
	String getCurrentPageUrl();
	
	String getNextPageUrl();
	
	String getPath();

	static RequestPaginator of(Paginator paginator, HttpServletRequest request, int pageSize, int currentPage) {
		return new HttpRequestPaginator(paginator, request, pageSize, currentPage);
	}

	static RequestPaginator of(Paginator paginator, HttpServletRequest request, int pageSize, int currentPage, String filter) {
		return new HttpRequestPaginator(paginator, request, pageSize, currentPage, filter);
	}
}
