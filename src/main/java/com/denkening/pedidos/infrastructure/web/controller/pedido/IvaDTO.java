package com.denkening.pedidos.infrastructure.web.controller.pedido;

import com.denkening.pedidos.infrastructure.database.detalleiva.DetalleIvaEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IvaDTO {
    private int tarifaIva;
    private Long valorBase;
    private Long valorIva;

    public static List<IvaDTO> fromEntity(List<DetalleIvaEntity> detalleIva){
        return detalleIva.stream().map(
            iva -> new IvaDTO(
                iva.getTarifaIva(),
                iva.getValorBase(),
                iva.getValorIva()
            )
        ).collect(Collectors.toList());
    }
}
