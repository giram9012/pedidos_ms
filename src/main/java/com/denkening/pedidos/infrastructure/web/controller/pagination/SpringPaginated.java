package com.denkening.pedidos.infrastructure.web.controller.pagination;

import com.denkening.pedidos.domain.pagination.Paginated;
import com.denkening.pedidos.domain.pagination.Paginator;
import org.springframework.data.domain.Page;

import java.util.List;


public class SpringPaginated<T> implements Paginated<T> {

	private final List<T> content;
	private final Paginator paginator;
	
	public static <E, T> Paginated<T> of(Page<E> paginated, List<T> content) {
		Paginator paginator = new SpringPaginator<>(paginated);
		return new SpringPaginated<>(content, paginator);
	}

	private SpringPaginated(List<T> content, Paginator paginator) {
		this.content = content;
		this.paginator = paginator;
	}
 	
	@Override
	public List<T> getContent() {
		return content;
	}

	@Override
	public Paginator getPaginator() {
		return paginator;
	}	
}
