package com.denkening.pedidos.infrastructure.web.controller.pedido;

import com.denkening.pedidos.application.pedido.cambiarestado.CambiarEstadoCommand;
import com.denkening.pedidos.application.pedido.cambiarestado.CambiarEstadoHandler;
import com.denkening.pedidos.application.pedido.hacerpedido.HacerPedidoCommand;
import com.denkening.pedidos.application.pedido.hacerpedido.HacerPedidoHandler;
import com.denkening.pedidos.domain.model.pedido.Pedido;
import com.denkening.pedidos.domain.pagination.Paginated;
import com.denkening.pedidos.domain.pagination.Paginator;
import com.denkening.pedidos.infrastructure.database.pedido.PedidoEntity;
import com.denkening.pedidos.infrastructure.database.pedido.PedidoRepositorySQL;
import com.denkening.pedidos.infrastructure.web.controller.pagination.RequestPaginator;
import com.denkening.pedidos.infrastructure.web.controller.response.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/pedidos")
public class PedidoController {

    private final HacerPedidoHandler hacerPedidoHandler;
    private final CambiarEstadoHandler cambiarEstadoHandler;
    private final PedidoRepositorySQL repoSQL;

    @Autowired
    public PedidoController(HacerPedidoHandler hacerPedidoHandler, CambiarEstadoHandler cambiarEstadoHandler, PedidoRepositorySQL repoSQL) {
        this.hacerPedidoHandler = hacerPedidoHandler;
        this.cambiarEstadoHandler = cambiarEstadoHandler;
        this.repoSQL = repoSQL;
    }

    @GetMapping("/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> buscarBy(@PathVariable String id) {
        PedidoEntity pedidoE = repoSQL.findEntityById(id).orElseThrow(
            () -> new EntityNotFoundException());
        return new ApiResponse()
            .status(HttpStatus.OK)
            .content(PedidoDTO.fromEntity(pedidoE))
            .build();
    }

    @PostMapping
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> hacerPedido(@RequestBody HacerPedidoCommand command) {
        Pedido pedido = hacerPedidoHandler.execute(command);
        return new ApiResponse().status(HttpStatus.CREATED).content(pedido.getId()).build();
    }

    @PatchMapping("/{id}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> cambiarEstadoPedido(@RequestBody CambiarEstadoCommand command,
                                                 @PathVariable("id") String id) {
        Pedido pedido = cambiarEstadoHandler.execute(command);
        return new ApiResponse()
            .status(HttpStatus.OK)
            .content(new PedidoDTO()
                .builder()
                .estado(pedido.getEstado().value())
                .id(pedido.getId().toString()).build()
            ).build();
    }

    @GetMapping
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> buscarByEstado(@RequestParam(name = "estado") String estado,
                                            @RequestParam(defaultValue = "0") int page,
                                            @RequestParam(defaultValue = "20") int size, HttpServletRequest request
    ) {
        Paginated<PedidoEntity> paginatedResult = repoSQL.findAllEntitiesByEstado(estado,  page, size);
        Paginator paginator = RequestPaginator.of(paginatedResult.getPaginator(), request, size, page, "estado="+estado+"&");
        List<PedidoDTO> productsDto = paginatedResult.getContent().stream().map(p -> PedidoDTO.fromEntity(p)).collect(Collectors.toList());
        return new ApiResponse().status(HttpStatus.OK).content(productsDto).paginator(paginator).build();
    }

}
