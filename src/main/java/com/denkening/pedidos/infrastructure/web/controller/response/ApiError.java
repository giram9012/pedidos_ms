package com.denkening.pedidos.infrastructure.web.controller.response;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.FieldError;

@Data
@NoArgsConstructor
public class ApiError {

    private String field;
    private String defaultMessage;
    private Object rejectedValue;

    public ApiError(FieldError fieldError) {
        this.field = fieldError.getField();
        this.defaultMessage = fieldError.getDefaultMessage();
        this.rejectedValue = fieldError.getRejectedValue();
    }

    public ApiError(String field, String defaultMessage, Object rejectedValue) {
        this.field = field;
        this.defaultMessage = defaultMessage;
        this.rejectedValue = rejectedValue;
    }
}
