package com.denkening.pedidos.infrastructure.web.controller.comprador;

import com.denkening.pedidos.infrastructure.database.comprador.CompradorEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CompradorDTO {

    private String id;
    private String nombre;
    private String telefono;
    private String direccion;

    public static CompradorDTO fromEntity(CompradorEntity e){
        return new CompradorDTO(
            e.getId(),
            e.getNombre(),
            e.getTelefono(),
            e.getDireccion()
        );

    }
}
