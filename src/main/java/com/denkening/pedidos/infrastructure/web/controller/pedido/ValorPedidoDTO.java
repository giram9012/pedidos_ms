package com.denkening.pedidos.infrastructure.web.controller.pedido;

import com.denkening.pedidos.infrastructure.database.valorpedido.ValorPedidoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValorPedidoDTO {
    private Long total;
    private Long subTotal;
    private Long totalIva;
    private List<IvaDTO> detalleIva;

    public static final ValorPedidoDTO fromEntity(ValorPedidoEntity valorPedidoEntity) {
        return new ValorPedidoDTO(
            valorPedidoEntity.getTotal(),
            valorPedidoEntity.getSubTotal(),
            valorPedidoEntity.getTotalIva(),
            IvaDTO.fromEntity(valorPedidoEntity.getDetallesIva())
        );
    }
}
