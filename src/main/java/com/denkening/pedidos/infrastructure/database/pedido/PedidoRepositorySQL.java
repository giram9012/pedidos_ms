package com.denkening.pedidos.infrastructure.database.pedido;

import com.denkening.pedidos.domain.model.estadopedido.Estado;
import com.denkening.pedidos.domain.model.pedido.Pedido;
import com.denkening.pedidos.domain.model.pedido.repository.PedidoRepository;
import com.denkening.pedidos.domain.pagination.Paginated;
import com.denkening.pedidos.infrastructure.database.comprador.CompradorEntity;
import com.denkening.pedidos.infrastructure.database.comprador.CompradorRepositorySQL;
import com.denkening.pedidos.infrastructure.database.detalleiva.DetalleIvaEntity;
import com.denkening.pedidos.infrastructure.database.detalleiva.DetalleIvaRepositorySQL;
import com.denkening.pedidos.infrastructure.database.estado.EstadoEntity;
import com.denkening.pedidos.infrastructure.database.estado.EstadoRepositorySQL;
import com.denkening.pedidos.infrastructure.database.producto.ProductoEntity;
import com.denkening.pedidos.infrastructure.database.producto.ProductoRepositorySQL;
import com.denkening.pedidos.infrastructure.database.valorpedido.ValorPedidoEntity;
import com.denkening.pedidos.infrastructure.database.valorpedido.ValorPedidoRepositorySQL;
import com.denkening.pedidos.infrastructure.web.controller.pagination.SpringPaginated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class PedidoRepositorySQL implements PedidoRepository {

    private final PedidoRepositoryJpaSpring repo;
    private final EstadoRepositorySQL estadoRepo;
    private final ValorPedidoRepositorySQL valorPedidoRepo;
    private final ProductoRepositorySQL productoRepo;
    private final DetalleIvaRepositorySQL detalleIvaRepo;
    private final CompradorRepositorySQL compradorRepo;

    @Autowired
    public PedidoRepositorySQL(PedidoRepositoryJpaSpring repo, EstadoRepositorySQL estadoRepo, ValorPedidoRepositorySQL valorPedidoRepo, ProductoRepositorySQL productoRepo, DetalleIvaRepositorySQL detalleIvaRepo, CompradorRepositorySQL compradorRepo) {
        this.repo = repo;
        this.estadoRepo = estadoRepo;
        this.valorPedidoRepo = valorPedidoRepo;
        this.productoRepo = productoRepo;
        this.detalleIvaRepo = detalleIvaRepo;
        this.compradorRepo = compradorRepo;
    }

    @Override
    public Pedido save(Pedido pedido) {

        EstadoEntity estadoEntityFound = estadoRepo.findByNombre(pedido.getEstado().value())
            .orElseThrow(() -> new EntityNotFoundException("No se encontró la entidad estado en la BD"));

        ValorPedidoEntity valorPedidoEntity = new ValorPedidoEntity(null, pedido.getValorPedido().getTotal(),
            pedido.getValorPedido().getSubTotal(), pedido.getValorPedido().getTotalIva(), null,
            null);
        ValorPedidoEntity valorPedidoEntitySaved = valorPedidoRepo.save(valorPedidoEntity);

        java.util.List<DetalleIvaEntity> detallesIvaEntity = DetalleIvaEntity.from(pedido.getValorPedido().getDetalleIva());
        java.util.List<DetalleIvaEntity> detallesIvaEntityComplete = detallesIvaEntity.stream().map(i -> {
            i.setValorPedido(valorPedidoEntitySaved);
            return i;
        }).collect(Collectors.toList());
        java.util.List<DetalleIvaEntity> detallesIvaEntitySaved = detalleIvaRepo.saveAll(detallesIvaEntityComplete);
        valorPedidoEntitySaved.setDetallesIva(detallesIvaEntitySaved);

        CompradorEntity compradorEntity = compradorRepo.findEntityById(pedido.getCompradorId().toString())
            .orElseThrow(() -> new EntityNotFoundException("No se encontró la entidad comprador en la BD"));
        compradorEntity.setDireccion(pedido.getDireccion());

        PedidoEntity pedidoEntity = new PedidoEntity();
        pedidoEntity.setId(pedido.getId().toString());
        pedidoEntity.setDireccion(pedido.getDireccion());
        pedidoEntity.setFechaCreacion(PedidoEntity.toLocalDateTime(pedido.getFechaCreacion()));
        pedidoEntity.setEstado(estadoEntityFound);
        pedidoEntity.setComprador(compradorEntity);
        pedidoEntity.setValorPedido(valorPedidoEntitySaved);

        PedidoEntity pedidoEntitySaved = repo.save(pedidoEntity);

        java.util.List<ProductoEntity> productosEntity = ProductoEntity.from(pedido.getProductos());
        java.util.List<ProductoEntity> productosEntityComplete = productosEntity.stream().map(p -> {
            p.setPedido(pedidoEntitySaved);
            return p;
        }).collect(Collectors.toList());

        productoRepo.saveAll(productosEntityComplete);

        return PedidoEntity.toDomain(pedidoEntitySaved);
    }

    @Override
    public Pedido update(String id, Estado estado) {

        PedidoEntity pedidoE = repo.findById(id).orElseThrow(() ->
            new EntityNotFoundException("No se encontro el pedido con id: " + id));
        EstadoEntity estadoEntity = estadoRepo.findByNombre(estado.value()).orElseThrow(
            () -> new EntityNotFoundException("No se encontro el estado " + estado.value()));
        pedidoE.setEstado(estadoEntity);
        return PedidoEntity.toDomain(pedidoE);
    }

    public Optional<PedidoEntity> findEntityById(String id) {
        Optional<PedidoEntity> e = repo.findById(id);
        return e.isPresent() ? Optional.of(e.get()) : Optional.empty();
    }

    public Paginated<PedidoEntity> findAllEntitiesByEstado(String estado, int page, int size) {
        EstadoEntity estadoE = estadoRepo.findByNombre(estado)
            .orElseThrow(() -> new EntityNotFoundException("No se encontro el estado")); //TODO
        Page<PedidoEntity> paginated = repo.findAllByEstado(estadoE, PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "fechaCreacion")));
        List<PedidoEntity> pedidos = paginated.getContent();
        return SpringPaginated.of(paginated, pedidos);
    }
}
