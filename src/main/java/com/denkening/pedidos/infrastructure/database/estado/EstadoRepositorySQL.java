package com.denkening.pedidos.infrastructure.database.estado;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public class EstadoRepositorySQL {

    private EstadoRepositoryJpaSpring repo;

    @Autowired
    public EstadoRepositorySQL(EstadoRepositoryJpaSpring repo) {
        this.repo = repo;
    }

    public EstadoEntity save(EstadoEntity estado){
        return repo.save(estado);
    }

    public Optional<EstadoEntity> findByNombre(String nombre){
        return repo.findByNombre(nombre);
    }
}
