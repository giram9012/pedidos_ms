package com.denkening.pedidos.infrastructure.database.comprador;

import com.denkening.pedidos.domain.model.comprador.Comprador;
import com.denkening.pedidos.domain.model.comprador.repository.CompradorRepository;
import com.denkening.pedidos.domain.pagination.Paginated;
import com.denkening.pedidos.infrastructure.database.estado.EstadoEntity;
import com.denkening.pedidos.infrastructure.database.pedido.PedidoEntity;
import com.denkening.pedidos.infrastructure.web.controller.pagination.SpringPaginated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Repository
public class CompradorRepositorySQL implements CompradorRepository {

    @Autowired
    private CompradorRepositoryJpaSpring repo;

    @Override
    public Comprador save(Comprador comprador) {
        CompradorEntity e = repo.save(CompradorEntity.fromDomain(comprador));
        return CompradorEntity.toDomain(e);
    }

    public Optional<CompradorEntity> findEntityById(String id) {
        Optional<CompradorEntity> e = repo.findById(id);
        return e.isPresent() ? Optional.of(e.get()) : Optional.empty();
    }

    public Optional<CompradorEntity> findEntityByTelefono(String telefono) {
        Optional<CompradorEntity> e = repo.findByTelefono(telefono);
        return e.isPresent() ? Optional.of(e.get()) : Optional.empty();
    }

    public Paginated<CompradorEntity> findAllEntitiesByTelefono(String telefono, int page, int size) {
        Page<CompradorEntity> paginated = repo.findAllByTelefono(telefono, PageRequest.of(page, size));
        List<CompradorEntity> compradores = paginated.getContent();
        return SpringPaginated.of(paginated, compradores);
    }
}
