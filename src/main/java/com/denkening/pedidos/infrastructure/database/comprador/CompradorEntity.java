package com.denkening.pedidos.infrastructure.database.comprador;

import com.denkening.pedidos.domain.model.comprador.Comprador;
import com.denkening.pedidos.infrastructure.database.pedido.PedidoEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity(name = "comprador")
@Table(name = "compradores")
@Data
@NoArgsConstructor
public class CompradorEntity {

    @Id
    private String id;

    @Column(nullable = false)
    private String nombre;

    @Column(nullable = false)
    private String telefono;

    private String direccion;

    @OneToMany(mappedBy = "comprador")
    private List<PedidoEntity> pedidos = new ArrayList<>();

    public static CompradorEntity fromDomain(Comprador c) {
        CompradorEntity compradorE = new CompradorEntity();
        compradorE.setId(c.getId().toString());
        compradorE.setNombre(c.getNombre());
        compradorE.setTelefono(c.getTelefono());
        return compradorE;
    }

    public static Comprador toDomain(CompradorEntity e) {
        try {
            UUID.fromString(e.getId());
            return new Comprador(UUID.fromString(e.getId()), e.getNombre(), e.getTelefono());
        } catch (Exception ex) {
            throw new RuntimeException("No fue posible convetir de comprador entity a dominio");
        }
    }
}
