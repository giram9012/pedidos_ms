package com.denkening.pedidos.infrastructure.database.comprador;

import com.denkening.pedidos.infrastructure.database.estado.EstadoEntity;
import com.denkening.pedidos.infrastructure.database.pedido.PedidoEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CompradorRepositoryJpaSpring extends JpaRepository<CompradorEntity, String> {

    Optional<CompradorEntity> findByTelefono(String telefono);

    Page<CompradorEntity> findAllByTelefono(String telefono, Pageable pageable);

}
