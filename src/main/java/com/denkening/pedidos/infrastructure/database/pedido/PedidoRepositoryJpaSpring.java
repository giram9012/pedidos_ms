package com.denkening.pedidos.infrastructure.database.pedido;

import com.denkening.pedidos.infrastructure.database.estado.EstadoEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PedidoRepositoryJpaSpring extends JpaRepository<PedidoEntity, String> {

    Page<PedidoEntity> findAllByEstado(EstadoEntity estadoEntity, Pageable pageable);
}
