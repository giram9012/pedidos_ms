package com.denkening.pedidos.infrastructure.database.detalleiva;

import com.denkening.pedidos.domain.model.iva.Iva;
import com.denkening.pedidos.domain.model.tarifaiva.IvaCero;
import com.denkening.pedidos.domain.model.tarifaiva.IvaCinco;
import com.denkening.pedidos.domain.model.tarifaiva.IvaDiecinueve;
import com.denkening.pedidos.domain.model.tarifaiva.TarifaIva;
import com.denkening.pedidos.infrastructure.database.valorpedido.ValorPedidoEntity;
import io.vavr.collection.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Entity(name = "detalleIva")
@Table(name = "detallesIva")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DetalleIvaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "detalles_iva_sequences")
    @SequenceGenerator(name = "detalles_iva_sequences", sequenceName = "SEQ_DETALLES_IVA", allocationSize = 1)
    private Long id;

    @Column(nullable = false)
    private int tarifaIva;

    @Column(nullable = false)
    private Long valorBase;

    @Column(nullable = false)
    private Long valorIva;

    @ManyToOne
    private ValorPedidoEntity valorPedido;

    public static java.util.List<DetalleIvaEntity> from(List<Iva> detalleIva) {
        return detalleIva.map(iva ->
            new DetalleIvaEntity(null,
                iva.getTarifaIva().value(),
                iva.getValorBase(),
                iva.getValorIva(),
                null)
        ).toJavaList();
    }

    public static List<Iva> toDomain(java.util.List<DetalleIvaEntity> detallesIvaEntity) {
        return List.ofAll(detallesIvaEntity).map(detalleIvaEntity ->
            new Iva(
                from(detalleIvaEntity.getTarifaIva()),
                detalleIvaEntity.getValorBase(),
                detalleIvaEntity.getValorIva()
            )
        );

    }

    public static TarifaIva from(int tarifaIva) {
        switch (tarifaIva) {
            case 0:
                return new IvaCero();
            case 5:
                return new IvaCinco();
            case 19:
                return new IvaDiecinueve();
            default:
                throw new RuntimeException("Iva invalida en la bd"); // TODO
        }
    }

}
