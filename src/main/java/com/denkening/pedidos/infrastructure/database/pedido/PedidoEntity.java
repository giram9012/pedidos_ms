package com.denkening.pedidos.infrastructure.database.pedido;

import com.denkening.pedidos.domain.model.estadopedido.Estado;
import com.denkening.pedidos.domain.model.pedido.Pedido;
import com.denkening.pedidos.domain.model.producto.Producto;
import com.denkening.pedidos.domain.model.valorpedido.ValorPedido;
import com.denkening.pedidos.infrastructure.database.comprador.CompradorEntity;
import com.denkening.pedidos.infrastructure.database.estado.EstadoEntity;
import com.denkening.pedidos.infrastructure.database.producto.ProductoEntity;
import com.denkening.pedidos.infrastructure.database.valorpedido.ValorPedidoEntity;
import io.vavr.collection.List;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.UUID;

@Entity(name = "pedido")
@Table(name = "pedidos")
@Data
@NoArgsConstructor
public class PedidoEntity {

    @Id
    private String id;

    @CreationTimestamp
    private LocalDateTime timeStampCreatedDate;

    @Column(nullable = false)
    private String direccion;

    @Column(nullable = false)
    private LocalDateTime fechaCreacion;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "estado_id", referencedColumnName = "id")
    private EstadoEntity estado;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "comprador_id", referencedColumnName = "id")
    private CompradorEntity comprador;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "valor_pedido_id", referencedColumnName = "id")
    private ValorPedidoEntity valorPedido;

    @OneToMany(mappedBy = "pedido", cascade = CascadeType.ALL, orphanRemoval = true)
    private java.util.List<ProductoEntity> productos = new ArrayList<>();

    public static Pedido toDomain(PedidoEntity pE) {
        Estado estado = EstadoEntity.toDomain(pE.getEstado());
        ValorPedido valorPedido = ValorPedidoEntity.toDomain(pE.getValorPedido());
        List<Producto> productos = ProductoEntity.toDomain(pE.getProductos());

        return new Pedido(
            UUID.fromString(pE.getId()),
            estado,
            toDateTime(pE.getFechaCreacion()),
            UUID.fromString(pE.getComprador().getId()),
            pE.getDireccion(),
            productos,
            valorPedido);
    }

    public static DateTime toDateTime(LocalDateTime localDTime) {
        return new org.joda.time
            .DateTime(localDTime.toInstant(ZoneOffset.UTC).toEpochMilli());
    }

    public static LocalDateTime toLocalDateTime(DateTime dateTime) {
        return LocalDateTime.of(
            dateTime.getYear(),
            dateTime.getMonthOfYear(),
            dateTime.getDayOfMonth(),
            dateTime.getHourOfDay(),
            dateTime.getMinuteOfHour(),
            dateTime.getSecondOfMinute(),
            dateTime.getMillisOfSecond()
        );
    }

}
