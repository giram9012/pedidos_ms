package com.denkening.pedidos.infrastructure.database.valorpedido;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ValorPedidoRepositoryJpaSpring extends JpaRepository<ValorPedidoEntity, Long> {

}
