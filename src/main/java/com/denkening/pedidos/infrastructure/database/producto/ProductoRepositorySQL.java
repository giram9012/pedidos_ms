package com.denkening.pedidos.infrastructure.database.producto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductoRepositorySQL {

    private ProductoRepositoryJpaSpring repo;

    @Autowired
    public ProductoRepositorySQL(ProductoRepositoryJpaSpring repo) {
        this.repo = repo;
    }

    public ProductoEntity save(ProductoEntity productoEntity) {
        return repo.save(productoEntity);
    }

    public List<ProductoEntity> saveAll(List<ProductoEntity> productos) {
        return repo.saveAll(productos);
    }
}
