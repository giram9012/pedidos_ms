package com.denkening.pedidos.infrastructure.database.producto;

import com.denkening.pedidos.domain.model.iva.Iva;
import com.denkening.pedidos.domain.model.producto.Producto;
import com.denkening.pedidos.infrastructure.database.detalleiva.DetalleIvaEntity;
import com.denkening.pedidos.infrastructure.database.pedido.PedidoEntity;
import io.vavr.collection.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.UUID;

@Entity(name = "producto")
@Table(name = "productos")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "productos_sequences")
    @SequenceGenerator(name = "productos_sequences", sequenceName = "SEQ_PRODUCTOS", allocationSize = 1)
    private Long id;

    @Column(nullable = false)
    private String productId;

    @Column(nullable = false)
    private String nombre;

    private String descripcion;

    @Column(nullable = false)
    private int cantidad;

    @Column(nullable = false)
    private Long precioUnidad;

    @Column(nullable = false)
    private int tarifaIva;

    @Column(nullable = false)
    private Long valorBase;

    @Column(nullable = false)
    private Long valorIva;

    @ManyToOne
    @JoinColumn(name = "pedido_id")
    private PedidoEntity pedido;

    public static java.util.List<ProductoEntity> from(List<Producto> productos) {

        return productos.map(producto ->
            new ProductoEntity(
                null,
                producto.getId().toString(),
                producto.getNombre(),
                producto.getDescripcion(),
                producto.getCantidad(),
                producto.getPrecioUnidad(),
                producto.getTarifaIva().value(),
                producto.getValorBase(),
                producto.getValorIva(),
                null
            )
        ).toJavaList();
    }

    public static List<Producto> toDomain(java.util.List<ProductoEntity> productosEntity) {
        return List.ofAll(productosEntity).map(pEntity ->
            new Producto(
                UUID.fromString(pEntity.getProductId()),
                pEntity.getNombre(),
                pEntity.getDescripcion(),
                pEntity.getCantidad(),
                pEntity.getPrecioUnidad(),
                new Iva(
                    DetalleIvaEntity.from(pEntity.getTarifaIva()),
                    pEntity.getValorBase(),
                    pEntity.getValorIva()
                )
            )
        );

    }


}
