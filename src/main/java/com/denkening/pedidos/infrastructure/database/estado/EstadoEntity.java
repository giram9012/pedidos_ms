package com.denkening.pedidos.infrastructure.database.estado;

import com.denkening.pedidos.domain.model.estadopedido.Anulado;
import com.denkening.pedidos.domain.model.estadopedido.Confirmado;
import com.denkening.pedidos.domain.model.estadopedido.Estado;
import com.denkening.pedidos.domain.model.estadopedido.Generado;
import com.denkening.pedidos.infrastructure.database.pedido.PedidoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "estado")
@Table(name = "estados")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EstadoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String nombre;

    @OneToMany(mappedBy = "estado")
    private List<PedidoEntity> pedido = new ArrayList<>();


    public static Estado toDomain(EstadoEntity estadoEntity) {
        switch (estadoEntity.nombre) {
            case "GENERADO":
                return new Generado();
            case "CONFIRMADO":
                return new Confirmado();
            case "ANULADO":
                return new Anulado();
            default:
                throw new RuntimeException("Estado de pedido no valida : "); // TODO
        }
    }
}
