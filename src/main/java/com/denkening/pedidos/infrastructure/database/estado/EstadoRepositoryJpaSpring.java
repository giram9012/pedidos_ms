package com.denkening.pedidos.infrastructure.database.estado;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EstadoRepositoryJpaSpring extends JpaRepository<EstadoEntity, Long> {
    Optional<EstadoEntity> findByNombre(String nombre);
}
