package com.denkening.pedidos.infrastructure.database.producto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepositoryJpaSpring extends JpaRepository<ProductoEntity, String> {

}
