package com.denkening.pedidos.infrastructure.database.detalleiva;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DetalleIvaRepositoryJpa extends JpaRepository<DetalleIvaEntity, Long> {

}
