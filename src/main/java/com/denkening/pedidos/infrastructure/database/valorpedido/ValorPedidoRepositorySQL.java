package com.denkening.pedidos.infrastructure.database.valorpedido;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ValorPedidoRepositorySQL {

    private ValorPedidoRepositoryJpaSpring repo;

    @Autowired
    public ValorPedidoRepositorySQL(ValorPedidoRepositoryJpaSpring repo) {
        this.repo = repo;
    }

    public ValorPedidoEntity save(ValorPedidoEntity valorPedidoEntity){
        return repo.save(valorPedidoEntity);
    }
}
