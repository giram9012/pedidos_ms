package com.denkening.pedidos.infrastructure.database.detalleiva;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class DetalleIvaRepositorySQL {

    private DetalleIvaRepositoryJpa repo;

    @Autowired
    public DetalleIvaRepositorySQL(DetalleIvaRepositoryJpa repo) {
        this.repo = repo;
    }

    public DetalleIvaEntity save(DetalleIvaEntity detalleIvaEntity) {
        return repo.save(detalleIvaEntity);
    }

    public List<DetalleIvaEntity> saveAll(List<DetalleIvaEntity> detallesIvaEntity) {
        return repo.saveAll(detallesIvaEntity);
    }
}
