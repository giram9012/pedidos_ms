package com.denkening.pedidos.infrastructure.database.valorpedido;

import com.denkening.pedidos.domain.model.iva.Iva;
import com.denkening.pedidos.domain.model.valorpedido.ValorPedido;
import com.denkening.pedidos.infrastructure.database.detalleiva.DetalleIvaEntity;
import com.denkening.pedidos.infrastructure.database.pedido.PedidoEntity;
import io.vavr.collection.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;


@Entity(name = "valorPedido")
@Table(name = "valorPedidos")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ValorPedidoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "valor_pedidos_sequences")
    @SequenceGenerator(name = "valor_pedidos_sequences", sequenceName = "SEQ_VALOR_PEDIDO", allocationSize = 1)
    private Long id;

    @Column(nullable = false)
    private Long total;

    @Column(nullable = false)
    private Long subTotal;

    @Column(nullable = false)
    private Long totalIva;

    @OneToMany(mappedBy = "valorPedido", cascade = CascadeType.ALL)
    private java.util.List<DetalleIvaEntity> detallesIva = new ArrayList<>();

    @OneToOne(mappedBy = "valorPedido")
    private PedidoEntity pedido;

    public static ValorPedido toDomain(ValorPedidoEntity valorPedidoE) {
        List<Iva> detalleIva = DetalleIvaEntity.toDomain(valorPedidoE.getDetallesIva());

        return new ValorPedido(
            valorPedidoE.getTotal(),
            valorPedidoE.getSubTotal(),
            valorPedidoE.getTotalIva(),
            detalleIva);
    }

}
