package com.denkening.pedidos.infrastructure.services;

import com.denkening.pedidos.domain.model.pedido.service.PedidoService;
import com.denkening.pedidos.infrastructure.database.pedido.PedidoRepositorySQL;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PedidoBeanService {

    @Bean
    public PedidoService pedidoService(PedidoRepositorySQL repo) {

        return new PedidoService(repo);
    }
}

