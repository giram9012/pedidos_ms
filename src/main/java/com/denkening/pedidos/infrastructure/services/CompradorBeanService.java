package com.denkening.pedidos.infrastructure.services;

import com.denkening.pedidos.domain.model.comprador.service.CompradorService;
import com.denkening.pedidos.infrastructure.database.comprador.CompradorRepositorySQL;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CompradorBeanService {

    @Bean
    public CompradorService compradorService(CompradorRepositorySQL repo) {

        return new CompradorService(repo);
    }
}
