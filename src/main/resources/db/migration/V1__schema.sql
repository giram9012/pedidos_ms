-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: localhost    Database: pedidos
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `compradores`
--

CREATE TABLE `compradores` (
  `id` varchar(255) NOT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL,
  `telefono` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `estados`
--
CREATE TABLE `estados` (
  `id` bigint NOT NULL,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `valor_pedidos`
--
CREATE TABLE `valor_pedidos` (
  `id` bigint NOT NULL,
  `sub_total` bigint NOT NULL,
  `total` bigint NOT NULL,
  `total_iva` bigint NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `detalles_iva`
--
CREATE TABLE `detalles_iva` (
  `id` bigint NOT NULL,
  `tarifa_iva` int NOT NULL,
  `valor_base` bigint NOT NULL,
  `valor_iva` bigint NOT NULL,
  `valor_pedido_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6jd547ssnpmy8b6cmku0bh3ft` (`valor_pedido_id`),
  CONSTRAINT `FK6jd547ssnpmy8b6cmku0bh3ft` FOREIGN KEY (`valor_pedido_id`) REFERENCES `valor_pedidos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Tables structure for sequences
--
CREATE TABLE `valor_pedidos_sequences` (
    `sequence_name` varchar(255) NOT NULL,
    `next_val` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `detalles_iva_sequences` (
    `sequence_name` varchar(255) NOT NULL,
    `next_val` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `productos_sequences` (
    `sequence_name` varchar(255) NOT NULL,
    `next_val` bigint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `pedidos`
--
CREATE TABLE `pedidos` (
  `id` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `fecha_creacion` tinyblob NOT NULL,
  `time_stamp_created_date` datetime(6) DEFAULT NULL,
  `comprador_id` varchar(255) DEFAULT NULL,
  `estado_id` bigint DEFAULT NULL,
  `valor_pedido_id` bigint DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK3go5hjo66n7je3bjgh3ao0nqg` (`comprador_id`),
  KEY `FKt05xt0uxbvpgpcclqwjjc31pq` (`estado_id`),
  KEY `FK2f5ib7wt2qnthkms349kccmiq` (`valor_pedido_id`),
  CONSTRAINT `FK2f5ib7wt2qnthkms349kccmiq` FOREIGN KEY (`valor_pedido_id`) REFERENCES `valor_pedidos` (`id`),
  CONSTRAINT `FK3go5hjo66n7je3bjgh3ao0nqg` FOREIGN KEY (`comprador_id`) REFERENCES `compradores` (`id`),
  CONSTRAINT `FKt05xt0uxbvpgpcclqwjjc31pq` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Table structure for table `productos`
--
CREATE TABLE `productos` (
  `id` varchar(255) NOT NULL,
  `cantidad` int NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) NOT NULL,
  `precio_unidad` bigint NOT NULL,
  `tarifa_iva` int NOT NULL,
  `valor_base` bigint NOT NULL,
  `valor_iva` bigint NOT NULL,
  `pedido_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKryrnyyfje4nmop09ex7k2nuvd` (`pedido_id`),
  CONSTRAINT `FKryrnyyfje4nmop09ex7k2nuvd` FOREIGN KEY (`pedido_id`) REFERENCES `pedidos` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-07-22  4:16:03