-- Estados
INSERT INTO `estados` (`id`, `nombre`) VALUES (1,'GENERADO'),(2,'CONFIRMADO'),(3,'ANULADO');

-- Sequences
INSERT INTO `valor_pedidos_sequences` (`sequence_name`, `next_val`) VALUES ('SEQ_VALOR_PEDIDOS',1);
INSERT INTO `detalles_iva_sequences` (`sequence_name`, `next_val`) VALUES ('SEQ_DETALLES_IVA',1);
INSERT INTO `productos_sequences` (`sequence_name`, `next_val`) VALUES ('SEQ_PRODUCTOS',1);
